var amqp = require('amqp');

var connection = amqp.createConnection({host: 'amqp://teams:prologic@10.64.33.101:5672'}, {defaultExchangeName:"teams"} );

var message = {
	"client":  "prol",
	"environment":  "pts01",
	"report":  "1099_report.7500.tar",
	"oldStyle":  false,
    "restart": false
}

connection.on('ready', function() {
	console.log('connected to rabbitMQ');
		connection.exchange('teams', {passive: true}, function (exchange){
				console.log('exchange ready ' + exchange.name);
						exchange.publish('cli.deploy-report', message, {contentType: 'application/json'}, function(){
							console.log('message published');
						})
				})
                
});


