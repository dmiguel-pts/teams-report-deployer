var express = require('express'),
	 http = require('http'),
	 path = require('path'),
	 request = require('request'),
	 bodyParser = require('body-parser');


var jdbc = new ( require('jdbc') );
var app = express();

var server = http.createServer(app);

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));
//configure jdbc
var config = {
	libpath: __dirname + '/public/jar/postgresql.jar',
	drivername: 'org.postgresql.Driver',
	url: 'jdbc:postgresql://ymir.ptsteams.local:5432/tms',

	user: 'tms',
	password: 'prologic'
};

jdbc.initialize(config, function(err,res){
	if(err) {
		console.log('init error: ' + err);
	} else if(res){
		console.log("res: " + res);
	}
});

app.get('/clients', function(request, response) {

		var gQ = function(err, results){
			if (err) {
				console.log(err);
			} else if (results){
				//console.log(results);
				response.send(results);
			} 

			jdbc.close(function(err){
				if(err){
					console.log(err);
				} else {
					console.log("connection closed successfully!");
				}
			});
		};

		var open = jdbc.open(function(err, conn){
			if(err){
				console.log("JDBC open:  " + err);
			}
			if (conn){
				//jdbc.executeQuery("SELECT DISTINCT report_type_code FROM public.report", gQ);
				jdbc.executeQuery("select * from public.report_revision inner join public.report on public.report_revision.report_name=public.report.report_name;", gQ);
				
			}
		});


});

app.get('/e', function(req, res){
	
	var url = "http://update.teams360.net/environments/rest/teams";

	request({
		url: url,
		json: true		
	},function(error, response, body){
			if(!error && response.statusCode == 200){
				str = JSON.stringify(body);
				str = str.replace(/\n/g, ' ');
				str = JSON.parse(str);
				console.log("sending rest data to angular");
				res.send(str);
			}
	})
})

app.get('/', function(request, response){
	response.send('hello world')
})

app.post('/myPost', function(request, response){
    var run =  require('./deploy.js');

    for(x in request.body){
        console.log("printing: " + JSON.stringify(request.body[x]))
        run.print(request.body[x]);
    }    

});

var server = app.listen(3000);

