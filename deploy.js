var amqp = require('amqplib');
var when = require('when');
var defer = when.defer;
var uuid = require('node-uuid');

        var m = {
            "client": "prol",
            "environment": "pts03",
            "report": "teams_reports.11336.tar",
            "oldStyle": false,
            "restart": false
        }

        var m1 = {
            "client": "prol",
            "environment": "pts05",
            "report": "teams_reports.11336.tar",
            "oldStyle": false,
            "restart": false
        }
        
        var list = []

            list.push(m);
            list.push(m1);

amqp.connect('amqp://teams:prologic@10.64.33.101:5672').then(function(conn){
    return when(conn.createChannel().then(function(ch){
        
        var answer = defer();
        var corrId = uuid();

        function maybeAnswer(msg){
            if(msg.properties.correlationId === corrId){
                answer.resolve(JSON.stringify(msg));
                console.log(msg.content.toString('utf8'));
            }
            
        }

        var q = 'cli-deploy-report';

        var ok = ch.assertQueue('', {exclusive: true})
            .then(function(qok){
               ch.bindQueue( qok.queue, 'teams', qok.queue, []); 
               return qok.queue 
            });

        ok = ok.then(function(queue){
            return ch.consume(queue, maybeAnswer, {noAck: true})
                .then(function(){ return queue }); //this gets called: resolve failed   
        });

        ok = ok.then(function(queue){

            ch.sendToQueue(q, new Buffer(JSON.stringify(list[x])), {
                correlationId: corrId, replyTo: queue   
            });

            return answer.promise;   
        });

        return ok.then(function(){
            console.log('finish')
        })

    })).ensure(function(){ conn.close(); });

}).then(null, console.warn);

