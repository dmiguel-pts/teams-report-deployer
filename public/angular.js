angular.module('reportApp', ['ui.bootstrap']);

angular.module('reportApp').controller('clients', ["$scope", "$http", "$rootScope", function($scope, $http, $rootScope){
		
        var url = 'https://update.teams360.net/operations';
		$scope.showClients = false;
		$scope.loading = true;
		$scope.grid = false;
		$rootScope.show_list = false;
        $scope.show_clear = true;
		$scope.select_all_pd = false;
		$scope.select_all_tr = false;
		$scope.select_all_ua = false;
		
		$scope.list_of_pd =[];
		$scope.list_of_tr = [];
		$scope.list_of_ua = [];

		$http.get(url +'/clients.json').
			success(function(data){
				$scope.clients = data;
				
				var i, j, 
				id = [],
				name = [],
				report = [];

				j = data.length;

				for(i=0; i < j; i++){
					var cur = data[i];

					if(id.indexOf(cur.report_type_code) < 0){
						id.push(cur.report_type_code);
					}
				}
			
				$scope.id = id.sort();
				$scope.name = name;
				$scope.report = report;
				$scope.loading = false;
				$scope.grid = true;
                $scope.reportClient  = 'Base';
                $scope.update();
			})

	$scope.update = function() {
	
		var k = $scope.clients.length;
		var base_name = [];
        
		for(i = 0; i < k; i++){
			var cur = $scope.clients[i];
			if(base_name.indexOf(cur.report_name) < 0 && cur.report_type_code === $scope.reportClient){
				base_name.push(cur.report_name);
			}
		}
		$scope.base_name = base_name.sort();
	}
	
	$scope.update2 = function () {
	
        var l = $scope.clients.length;
		var rev = [];

		for(i=0; i < l ; i ++){
			var cur = $scope.clients[i];
			if(cur.report_name === $scope.reportName){
				rev.push(cur.report_revision);
			}	
		}
		$scope.rev = rev.sort(function(a,b){return  b-a});
	}

	$scope.update4 = function() {
		$http.get(url +"/e").
                success(function (data){
                    function SortByClient(x, y) {
                        return ((x.client_code < y.client_code) ? 1 : ((x.client_code > y.client_code) ? 1 : 0));
                    }
                    
                    $scope.envs = data.teamsInstanceModel.sort(SortByClient);
                    $scope.environment_list = {};
                    $scope.client_list = [];

                    for(var env in $scope.envs){

                        var client = $scope.envs[env].client_code;
                        var env_type = $scope.envs[env].env_type;
                        var env_name = $scope.envs[env].environment

                        if($scope.environment_list[client]){
                            
                            var len = $scope.environment_list[client].length;
                            var is_in = false;

                            for(i = 0; i < len; i ++){
                                if($scope.environment_list[client][i].type === env_type){
                                    is_in = true;
                                }
                                
                                if($scope.envs[env].env_type === 'pd'){
                                    var pd_message = {
                                        "client" : $scope.envs[env].client_code,
                                        "environment" : $scope.envs[env].env_type,
        
                                    }

                                    $scope.list_of_pd.push(pd_message);
                                }

                                if($scope.envs[env].env_type === 'tr'){
                                    var tr_message = {
                                        "client" : $scope.envs[env].client_code,
                                        "environment" : $scope.envs[env].env_type,
                                    }

                                    $scope.list_of_tr.push(tr_message);
                                }

                                if($scope.envs[env].env_type === 'ua'){
                                    var ua_message = {
                                        "client" : $scope.envs[env].client_code,
                                        "environment" : $scope.envs[env].env_type,
                                    }

                                    $scope.list_of_ua.push(ua_message);
                                }
                            }

                            if(is_in === false){
                                $scope.environment_list[client].push({type: env_type})
                            }

                        }else if(!$scope.environment_list[client]){
                            var list = [{type: env_type }];
                            $scope.environment_list[client] = list;
                        }	
                    }
        
            $scope.show_pd = true;    
            $scope.showClients = true;
            $scope.checkModel = {
                left: false,
                middle: false,
                right: false
            };
        
            
            //SELECT ALL PD ENVIRONMENTS
            $scope.click_l_hold = $scope.checkModel.left;

            $scope.click_left  = function() {
                
                var len = $scope.list_of_pd.length;
                var len_client_list = $rootScope.report_client_list.length;

                if(!$scope.click_l_hold){
                    $scope.add_pd();
                }

                if($scope.click_l_hold){
                    
                    $scope.temp = [];
                    for(i=0; i < len_client_list; i++){
                        if($rootScope.report_client_list[i].environment != 'pd'){
                            $scope.temp.push($rootScope.report_client_list[i]);
                        }
                    }

                    $rootScope.report_client_list = $scope.temp;
                    $rootScope.show_list = false;
                }
                $scope.click_l_hold = !$scope.click_l_hold;
            }

            
            $scope.show_key = function(key){
    
                var i;
                var len = $scope.environment_list[key].length;

                for(i=0; i < len; i++){
                    $scope.toggleActive(key, $scope.environment_list[key][i].type);
                }

            }

            $scope.isActive = false;
            
            //*******	Creating list of clients and enviroments to deploy to
            $rootScope.report_client_list = []; 

                $scope.toggleActive = function(a, b) {


                    $rootScope.show_list = true;

                    var in_list = false;
                    
                    var message = {
                        "client" : a,
                        "environment" : b,
                        "report" : $scope.reportName,
                        "version": $scope.reportVersion,
                        "type": $scope.reportClient
                    }
                    
                    var i = 0;
                    var z = $rootScope.report_client_list.length;

                    for(i=0; i < z; i++){
                        var cur = $rootScope.report_client_list[i];
                        if( message.client == cur.client && message.environment == cur.environment){
                            in_list = true;
                            $scope.remove(i);
                        }
                    }


                    if(!in_list){
                        $rootScope.report_client_list.push(message);	
                        $rootScope.set_color(message.client, message.environment)
                    }
                   

                    function sortByKey(array, key){
                        return array.sort(function(a, b){
                            var x = a[key];
                            var y = b[key];

                            return ((x < y) ? -1 : ((x>y) ? 1 : 0));
                        });
                    }

                    sortByKey($rootScope.report_client_list, "client");

                }//end toggle active `

                $scope.remove = function(idx){
                    var item = $rootScope.report_client_list[idx];
                    
                    $rootScope.report_client_list.splice(idx, 1);

                    if($rootScope.report_client_list.length === 0){
                        $rootScope.show_list = false;
                    }
                }

                $scope.add_pd = function(){
                    var i;
                    var k;
                    
                    
                    for(i=0; i < $scope.list_of_pd.length; i++){
                        var  m = {
                            "client": $scope.list_of_pd[i].client,
                            "environment": $scope.list_of_pd[i].environment,
                            "report": $scope.reportName, 
                            "type" : $scope.reportClient,
                            "version": $scope.reportVersion
                        }

                        $scope.flag = false;

                        if($rootScope.report_client_list.length == 0){
                            $rootScope.report_client_list.push(m)

                        }else{
                            for(k =0; k < $rootScope.report_client_list.length; k++){
                                if($scope.list_of_pd[i].client == $rootScope.report_client_list[k].client && $scope.list_of_pd[i].environment == $rootScope.report_client_list[k].environment){
                                    $scope.flag = true;
                                    break;
                                }
                            }
                            if(!$scope.flag){
                                $rootScope.report_client_list.push(m);
                            }
                        }
                    }

                    function sortByKey(array, key){
                        return array.sort(function(a, b){
                            var x = a[key];
                            var y = b[key];

                            return ((x < y) ? -1 : ((x>y) ? 1 : 0));
                        });
                    }

                    sortByKey($rootScope.report_client_list, "client");

                    $rootScope.show_list = true;

                }

                $scope.remove_pd = function(){
                    var i;
                    
                    for(i=0; i < $rootScope.report_client_list.length; i++){
                        if($rootScope.report_client_list[i].environment == 'pd'){
                            $rootScope.report_client_list.splice(i, 1);
                        }
                    }

                    if($rootScope.report_client_list.length == 0){
                        $rootScope.show_list = false;
                    }

                }
                
                $rootScope.set_color = function(key, type){
                    var found = false;
                    var len = $rootScope.report_client_list.length;

                    for(i=0; i < len; i++){
                        var cur = $rootScope.report_client_list[i];

                        if(key == cur.client && type == cur.environment){
                            return 'btn-success';
                        }
                    }
                
                    if(!found){
                        return 'btn-info';
                    }

                }

        }); //end http.get.success 
    }//end update4
}]);

angular.module('reportApp').controller('myTest', ["$scope", "$http", "$modal","$rootScope", function($scope, $http, $modal, $rootScope){

    var url = 'https://update.teams360.net/operations';

	$scope.click_test = function(){
        console.log($rootScope.report_client_list);
 		var res = $http.post(url + '/myPost', $rootScope.report_client_list);	
		
		res.success(function(data, status, headers, config){
			
		})

        var modalInstance = $modal.open({
            templateUrl: 'deployModal.html',
            controller: 'modalInstance',
            size: 'lg',
        })

        $rootScope.show_list = false;
        $scope.reset_list();

	}

    $scope.reset_list = function() {
        var i;

        for(i=0; i < $rootScope.report_client_list.length;i++){
            var cur = $rootScope.report_client_list[i];
            $rootScope.set_color(cur.client, cur.environment);
        }

        $rootScope.report_client_list = [];
        $rootScope.show_list = false
    }    
}]);

angular.module('reportApp').controller('modalInstance', ["$scope", "$modalInstance", function($scope, $modalInstance){
 
    $scope.ok = function($scope){
        $modalInstance.close();
    }
}])
