angular.module('deployedApp', ['ui.bootstrap']);

angular.module('deployedApp').controller('deploymentsCtrl', ["$scope", "$http", function($scope, $http){
    
     var url = 'https://update.teams360.net/operations/deployments';

    $http.get(url).
        success(function(data){
            $scope.reportStatus = data;

            var i;
            var j = data.length;
            $scope.reports = [];
            
            var response = {
                client: undefined,
                environment: undefined,
                statusOf: undefined,
                reportName: undefined,
                time: undefined
            }

            for(i=0; i < j;i++){ 
                var cur = data[i];

                var str = cur.replace(/\\/g, '');
               // var str = cur;
                var jsonMessage = undefined;  

                var splitString = str.split('***');

                //get client and environment

                response.client = splitString[0].split(' ')[7].split('-')[0];
                response.environment = splitString[0].split(' ')[7].split('-')[1];

                var time = splitString[0].split(' ');
                response.time = time[0] + ' ' + time[1] + ' ' + time[2] + ' ' + time[3];

                response.reportName = splitString[2].split('.');
                response.reportName = response.reportName[0] + '.' + response.reportName[1]; 


                if(splitString[1].indexOf('failure') > -1){
                    response.statusOf = 'failure';
                }
                else{
                    response.statusOf = 'success';
                }
               

                $scope.reports.push(response);
                
                response = {
                    client: undefined,
                    environment: undefined,
                    statusOf: undefined,
                    reportName: undefined,
                    time: undefined
                }
            }
        });
}])
