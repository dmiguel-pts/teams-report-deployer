var express = require('express'),
	http = require('http'),
	path = require('path'),
	request = require('request'),
	bodyParser = require('body-parser'),
    amqp = require('amqplib'),
    when = require('when'),
    defer = when.defer,
    uuid = require('node-uuid'),
    fs = require('fs'),
    ini = require('ini'),
    redis = require('redis');


var jdbc = new ( require('jdbc') );
var app = express();

var file = 'log/message.log';

var server = http.createServer(app);

var client = redis.createClient();


client.on('connect', function(){
    console.log('redis connected');   
})
    
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));
//configure jdbc
var config = {
	libpath: __dirname + '/public/jar/postgresql.jar',
	drivername: 'org.postgresql.Driver',
	url: 'jdbc:postgresql://ymir.ptsteams.local:5432/tms',

	user: 'tms',
	password: 'prologic'
};

jdbc.initialize(config, function(err,res){
	if(err) {
		console.log('init error: ' + err);
	} else if(res){
		console.log("res: " + res);
	}
});

app.get('/new_report', function(request, response){
    response.send("generating new file");
    var writeResults = function(err, results){
        if(err){
            console.log(err);
        } else if (results){
            var result_string = JSON.stringify(results);
            console.log(new Date() + ' creating temp_clients.json');
            fs.writeFile(__dirname +'/public/temp_client.json',result_string, function(err){
                if(err){
                    console.log(err);
                    jdbc.close(function(err){
                        if(err) console.log(err);
                    });
                } else {
                    console.log('moving report');
                    fs.rename('public/temp_client.json','public/clients.json');
                    jdbc.close(function(err){
                        if(err){
                            console.log(err);
                        } else {
                            console.log(new Date().toISOString() + " ymir db connection closed successfully!");
                        }
                    });

                }
            })
        }

        
    }

    var open = jdbc.open(function(err, conn){
        if(err){
            console.log("JDBC open:  " + err);
        }
        if (conn){
            //jdbc.executeQuery("SELECT DISTINCT report_type_code FROM public.report", gQ);
            jdbc.executeQuery("select * from public.report_revision inner join public.report on public.report_revision.report_name=public.report.report_name;", writeResults);
            
        }
    });
    

})

app.get('/clients', function(request, response) {
		var gQ = function(err, results){
			if (err) {
				console.log(err);
			} else if (results){
				//console.log(results);
				response.send(results);
			} 

			jdbc.close(function(err){
				if(err){
					console.log(err);
				} else {
					console.log("connection closed successfully!");
				}
			});
		};

		var open = jdbc.open(function(err, conn){
			if(err){
				console.log("JDBC open:  " + err);
			}
			if (conn){
				//jdbc.executeQuery("SELECT DISTINCT report_type_code FROM public.report", gQ);
				jdbc.executeQuery("select * from public.report_revision inner join public.report on public.report_revision.report_name=public.report.report_name;", gQ);
				
			}
		});


});

var teamsUrl = 'https://update.teams360.net/operations';

app.get('/e', function(req, res){
	
	var url = "http://update.teams360.net/environments/rest/teams";

	request({
		url: url,
		json: true		
	},function(error, response, body){
			if(!error && response.statusCode == 200){
				str = JSON.stringify(body);
				str = str.replace(/\n/g, ' ');
				str = JSON.parse(str);
				console.log("sending environments/rest/teams data to angular");
				res.send(str);
			}
	})
})

app.get('/', function(request, response){
	response.send('hello world')
})

app.get('/deployments', function(request, response){
    client.lrange("report_deployments", 0 , 1000, function(error, items){
        if(error) throw error
        response.send(items);
    });

})

app.post('/myPost', function(request, response){


    var print = function(message_details){

            var report = undefined;
            if(message_details.client.indexOf("Prologic") > -1){
                message_details.client = "PROL";
            }

            //create a report name based on whether its custom or not
            if(message_details.type.indexOf("Base") > -1){
                report = message_details.report + "." +  message_details.version + ".tar";
            }else{
                report = message_details.report + "." + message_details.type + "." + message_details.version + ".tar";
            }

            var c = message_details.client

            var  m = {
                "client": message_details.client,
                "environment": message_details.environment,
                "report": report, 
                "oldStyle": false,
                "restart": false
            }
            console.log(new Date() + ' message m: ' + JSON.stringify(m))
            var config = ini.parse(fs.readFileSync('/etc/teams/web', 'utf-8'));


            if(message_details.environment === 'pts01' || message_details.environment === 'pts02'|| message_details.environment === 'pts03' || message_details.environment === 'pts04' || message_details.environment === 'pts05'|| message_details.environment === 'pts06' || message_details.environment === 'pts07'){
                address = eval('config.addresses.QA');
                console.log(address);
            } else if ((message_details.environment === 'tr' || message_details.environment === 'ua') && message_details.client === 'ELPA') {
                address = '172.18.1.238';
            }else {
                address = eval('config.addresses.'+ c);
            }

            amqp.connect('amqp://teams:prologic@'+ address +':5672').then(function(conn){
                return when(conn.createChannel().then(function(ch){
                    var answer = defer();
                    var corrId = uuid();
                    function maybeAnswer(msg, foo){
                        if(msg.properties.correlationId === corrId){
                            answer.resolve(JSON.stringify(msg));
                            console.log(message_details.client + "-" + message_details.environment + ": " +msg.content.toString('utf8') + '*** ' + m.report);
    
                            var messageToString = msg.content.toString('utf8');
                            var messageToJSON = JSON.parse(messageToString);
                            var reportName = m.report.replace('.tar', '');
                            messageToJSON.report_name = reportName;
                            messageToString = JSON.stringify(messageToJSON);

                        
                            
                            
                            //console.log('@@@@TESTING: ' + messageToString);
                            var record = msg.content.toString('utf8');
                            var record = record.replace(/\\/g, "");

                            client.lpush(['report_deployments',new Date() + ' ' + message_details.client + '-' + message_details.environment + '***' + record + '***' + m.report], function(err, reply){
                                console.log("REDIS reply: " + reply);   
                            });

                            //client.lpush(['logstash-report_deployments',new Date() + ' ' + message_details.client + '-' + message_details.environment + ' ' + record + ' ' + m.report], function(err, reply){
                            //});
                            client.lpush(['logstash-report_deployments', messageToString], function(err, reply){
                            });
                            var date = new Date().toISOString();
                            res = date + ' - ' + message_details.client + " - " + message_details.environment + ": " +msg.content + '\n';
                            fs.appendFile(file,res,function(err){
                                if(err){
                                    console.error(err);
                                }   
                                console.log("ampq message response has been logged");
                                
                            })
                            function foo(res){
                                return res;
                            }
                        }
                    } 

                    var q = 'cli-deploy-report';

                    var ok = ch.assertQueue('', {exclusive: true})
                        .then(function(qok){
                           ch.bindQueue( qok.queue, 'teams', qok.queue, []); 
                           return qok.queue 
                        });
                    ok = ok.then(function(queue){
                        return ch.consume(queue, maybeAnswer, {noAck: true})
                            .then(function(){ return queue }); //this gets called: resolve failed   
                    });
                    ok = ok.then(function(queue){
                        ch.sendToQueue(q, new Buffer(JSON.stringify(m)), {
                            correlationId: corrId, replyTo: queue   
                        });
                        return answer.promise;   
                    });
                 return ok;
                })).ensure(function(){ conn.close(); console.log("amqp message connection closed"); });
            }).then(null, console.warn);
    }
   
    send_pd = function () {
        var found = false;
        for(x in request.body){
            fs.appendFile('printing',"printing: " + JSON.stringify(request.body[x]))
            
            if(request.body[x].environment == 'pd'){
                found = true;
                print(request.body[x]) ;
            }
        }    
    
        if(found){
          setTimeout(function(){
              send_ua();
          }, 10000);  
        }
        else{
            send_ua();
        }
    };

    send_ua = function(){
        var found = false;
        for(x in request.body){
            fs.appendFile('printing', "printing" + JSON.stringify(request.body[x]))
            
            if(request.body[x].environment == 'ua'){
                found = true;
                print(request.body[x]);
            }
        }
        if(found){      
            setTimeout(function(){
               send_tr();
            }, 10000);  
        }
        else{
            send_tr();
        }
    }

    send_tr = function(){
        var found = false;
        for(x in request.body){
            
            if(request.body[x].environment == 'tr'){
                found = true;
                print(request.body[x]);
            }
        }
        if(found){
            setTimeout(function(){
                send_else();
            }, 10000);
        }
        else{
            send_else();
        }
    }

//    send_else = function(){
//        for(x in request.body){
//            var cur = request.body[x].environment;
//            if(cur != 'pd' && cur != 'ua' && cur != 'tr'){
//                setTimeout(function(){
//                    console.log(new Date() + ' "In send else: request.body[x] = : " ' + JSON.stringify(request.body[x]))
//                   print(request.body[x]); 
//                },10000);
//            }
//        }
//
//    }
    send_else = function(){
        for(x in request.body){
            var cur = request.body[x].environment;
            if(cur != 'pd' && cur != 'ua' && cur != 'tr'){
                console.log(new Date() + ' "In send else: request.body[x] = : " ' + JSON.stringify(request.body[x]))
                print(request.body[x]);
            }
        }

    }


    send_pd();
});

var server = app.listen(3000);

